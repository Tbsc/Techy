## License

CoFHLib is a library made by Team CoFH, and on their GitHub page (https://github.com/CoFH/CoFHLib),
they say this is a library made for mod developers, and it is licensed under the LGPLv3, and therefore
redistributing is allowed and encouraged. It also means that I *am* allowed and encouraged to modify
the code as I wish, which I did to make it compatible with Minecraft 1.8.9.

Techy is license under the same license, the LGPLv3, and therefore the licences are compatible (obviously)