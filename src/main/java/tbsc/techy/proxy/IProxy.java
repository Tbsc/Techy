package tbsc.techy.proxy;

/**
 * Base interface for proxy
 *
 * Created by tbsc on 3/27/16.
 */
public interface IProxy {

    void initModels();

    void preInit();

}
