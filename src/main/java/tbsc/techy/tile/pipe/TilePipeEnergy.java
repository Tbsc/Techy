package tbsc.techy.tile.pipe;

/**
 * Tile entity for energy pipes.
 *
 * Created by tbsc on 5/9/16.
 */
public class TilePipeEnergy extends TilePipeBase {

    // Empty for now, everything is already done in base class

}
