package tbsc.techy.api;

import cofh.api.item.IToolHammer;

/**
 * Used to declare items as wrenches.
 * Supports the Ender IO wrench API using FML's optional API
 *
 * Created by tbsc on 4/26/16.
 */
public interface ITechyWrench extends IToolHammer {

}
