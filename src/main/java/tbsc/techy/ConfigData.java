package tbsc.techy;

/**
 * Stores any data that may be changed in config.
 * Values that are already here are default values.
 *
 * Created by tbsc on 4/3/16.
 */
public class ConfigData {

    public static int furnaceDefaultCookTime = 120;
    public static int furnaceDefaultEnergyUsage = 4000;

    public static int crusherDefaultProceessTime = 100;
    public static int crusherDefaultEnergyUsage = 5000;

}
