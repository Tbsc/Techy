package tbsc.techy.recipe;

/**
 * Interface for recipe input to implement
 *
 * Created by tbsc on 5/5/16.
 */
public interface IRecipeInput<T> {

    T getInput();

}
