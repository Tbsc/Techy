# Techy
Tech mod for 1.8.9

NOTE: Main repository is hosted on [GitLab](https://gitlab.com/Tbsc/Techy), but the repositories are set to be mirrored. Only thing is that I'll check GitLab more than GitHub, so if you want to catch me, go to [Reddit](https://reddit.com/u/Tbsc_) or to [GitLab](https://gitlab.com/Tbsc).

License: LGPLv3. TL;DR: You can copy, distribute, link proprietary code and modify the software as long as you track changes in source files. Any modifications to or software including (via compiler) my code must also be made available under the GPL license too.
